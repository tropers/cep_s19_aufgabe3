/*#############################################################################
 *###
 *###   Demo for CE related to lab exercise "DAC-Ansteuerung mit ISR"
 *###   ===========
 *#############################################################################
 *
 * VCS: git@bitbucket.org/schaefers/CE-Demo-Curve.git
 *
 * Developed on/for:
 *    TI CE Board (SS15 (resp. SS14))  with
 *      Silica Xynergy-M4 Board  with
 *      STM32F417
 *    using
 *      CEPtemplate (SS15 (resp. 2014/05/08))
 *      Keil �Vision V5.11.1.0
 *
 * Code belongs to TI4 CE; Informatik; HAW-Hamburg; Berliner Tor 7; D-20099 Hamburg
 * Code was based on demo examples from Silke Behn, Heiner Heitmann & Bernd Schwarz
 * Code was "mixed up" by Michael Sch�fers and was originally designed for Digilent Nexys2 Board
 * Code was adapted/ported to Silica Xynergy Board by Yannic Wilkening
 * Code was "pimped up" by Michael Sch�fers (e.g. adding comments and references to documentation)
 *
 *-----------------------------------------------------------------------------
 * Description:
 * ============
 * Code is an example for interaction of interrupts, timer and DAC.
 * Waveform with 7.5ms period (~133.3Hz) has to be send out over DAC.
 * Frequency of the (individual waveform) samples shall be 48KHz.
 *
 * The task/main loop has to compute signals and to store them in a fifo buffer.
 *     As metapher for the signal computation the task has to read out an appropriate sample ...
 *     of a signal table containing 1013 waveform samples (taken in equidistant time steps).
 * Timer8 has to signal via IRQ that it is time to send the next sample to DAC (with 48KHz frequency ;-)
 * The (this way) triggered ISR has to hand over the sample (from fifo) to DAC.
 *
 * LED7 on the CEP board is connected to GPIO I.7 of �C and used to mark that NO fifo underflow has occured
 * LED6 on the CEP board is connected to GPIO I.6 of �C and used to mark an Error resulting out of a fifo underflow
 * LED5 on the CEP board is connected to GPIO I.5 of �C and used to mark a spurious interrupt
 *
 * S8 toggle timer (either start or stop)
 * S7 acknowledges error detection by clearing LED6&5 and setting LED7
 * S6 clear LED7&6&5 (to controll that they are working)
 * S5 set LED7&6&5 (to controll that they are working)
 * S4 trap computation by busy waiting that S4 is released  producing fifo underflow this way
 *
 *-----------------------------------------------------------------------------
 * Abbreviations:
 * ==============
 *
 * DS ::= (STM32F415xx,) STM32F417xx Data Sheet ;   DM00035129 ;  DocID_022063 Rev5 ;  2015-03
 * PM ::= Programming Manual PM0214 ;               DM00046982 ;  DocID 022708 Rev4 ;  2014-04
 * RM ::= Reference Manual   RM0090 ;               DM00031020 ;  DocID 018909 Rev9 ;  2015-03
 * ~~~~~~
 * Guide ::= The Definitive Guide to ARM Cortex-M3 and Cortex-M4 Processors ;  Yiu,Joseph ;  3ed
 *
 *-----------------------------------------------------------------------------
 * History:
 * ========
 *   150423-150426: code ported to Silica Xynergy Board by Yannic Wilkening & Florian Meyer (see git@BitBucket.org/blamaster/demo-code-ce-a4.git)
 *   150427-150520: adding comments, references to documentation and negletable code changes by Michael Sch�fers
 *   For "full history" see ReadMe.txt (git@BitBucket.org/schaefers/CE-Demo-Curve.git)
 *-----------------------------------------------------------------------------
 * KNOWN PROBLEMS: none
 * NOTEs:          none
 *                 respectively:
 *                      Wann englisch und wann deutsch?
 *                      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *                      Grundsaetzlich gilt, wer C-Code fuer einen STM32 liest, der sollte auch C und STM32 kennen.
 *                      Dies ist aber ein Demo-Code, der auch "Dinge" erklaeren soll.
 *                      Kommentar/Erklaerungen der/die deswegen hier sind, werden in deutsch geschrieben
 *                      Kommentar, den auch ein "normales Programm" aufweisen sollte, hingegen in englisch.
 *                      {>...} markiert Referenzen auf Dokumentation oder eingebundene Dateien
 * OPEN POINTS:    Should all be removed in the final version, anyway they have been marked with: _???_<YYMMDD>  (search/grep for _???_)
 */



// current code version - increment with each change
#define  THE_VERY_C_CODE_VERSION  "v2.007"

/*
 * Header-Dateien, die benoetigt werden
 */
#include <stdint.h>
#include <stm32f4xx.h>
#include <stm32f4xx_rcc.h>
#include <stm32f4xx_gpio.h>
#include "CE_Lib.h"
#include "tft.h"

// comment line if you do NOT want to use TFT display
#define TFT_ENABLE

// comment line if you do NOT want to implement a final catch for safety's sake
//#define FINAL_CATCH_FOR_SAFETYS_SAKE_ENABLE


/*
 * macro converts binary value (containing up to 8 bits resp. <=0xFF) to unsigned decimal value
 * macro does NOT check for an invalid argument at all
 *
 * Das Macro ist Ersatz fuer fehlendes 0b-Prefix,  wie es ab GCC4.3 unterstuetzt und seit C++14 eingefordert wird.
 * Dieses Macro unterstuetzt NUR Werte bis zu 8Bit.
 * Sinn des Macros ist es Bitfelder in ihrer Groesse kenntlich zu machen. Also z.B. b(00111) fuer den 5Bit-Wert: dezimal 7.
 *
 * Die Syntax fuer dieses Macro ist nicht einfach. Sie muessen die Syntax NICHT verstehen. Sie muessen das Macro NUR anwenden koennen.
 * Wie auch immer:
 *   "##" ist ein "token-pasting"-Operator oder auch "zusammenfuehrender"-Operator.
 *   Beispielsweise nach einem Aufruf "b(010)" hat der Macro-Parameter "n" den Wert "010"
 *   und in diesem Fall wird"0x##b##ul" zu "0x010ul".
 *   Hierbei steht "0x" wie gewohnt fuer Hex und "ul" fuer "unsigned long".
 * Noch einmal, Sie muessen dieses Macro NUR anwenden koennen.
 */
#define b(n) (                                               \
    (unsigned char)(                                         \
            ( ( (0x##n##ul) & 0x00000001 )  ?  0x01  :  0 )  \
        |   ( ( (0x##n##ul) & 0x00000010 )  ?  0x02  :  0 )  \
        |   ( ( (0x##n##ul) & 0x00000100 )  ?  0x04  :  0 )  \
        |   ( ( (0x##n##ul) & 0x00001000 )  ?  0x08  :  0 )  \
        |   ( ( (0x##n##ul) & 0x00010000 )  ?  0x10  :  0 )  \
        |   ( ( (0x##n##ul) & 0x00100000 )  ?  0x20  :  0 )  \
        |   ( ( (0x##n##ul) & 0x01000000 )  ?  0x40  :  0 )  \
        |   ( ( (0x##n##ul) & 0x10000000 )  ?  0x80  :  0 )  \
    )                                                        \
)


/*
 * simplified access to switches S0 - S7
 */
#define  S8   ( !(GPIOH->IDR & (b(1) << 15)) )
#define  S7   ( !(GPIOH->IDR & (b(1) << 12)) )
#define  S6   ( !(GPIOH->IDR & (b(1) << 10)) )
#define  S5   ( !(GPIOF->IDR & (b(1) << 8 )) )
#define  S4   ( !(GPIOF->IDR & (b(1) << 7 )) )
#define  S3   ( !(GPIOF->IDR & (b(1) << 6 )) )
#define  S2   ( !(GPIOC->IDR & (b(1) << 2 )) )
#define  S1   ( !(GPIOI->IDR & (b(1) << 9 )) )



/*
 * Achtung:
 * Die in "misc.h" definierten Token "NVIC_PriorityGroup_#" sind NICHT geeignet
 * fuer die in "core_cm3.h" definierte Funktion "NVIC_SetPriorityGrouping()".
 * "NVIC_SetPriorityGrouping()" erwartet nur das BitFeld selbst.
 * Die Token definieren jedoch das BitFeld bereits an der "richtigen Position" (also einschliesslich niederwertigerer Bits).
 *
 * Das Prefix "STM32" markiert die nachfolgend selbstdefinierten Token, die nur das BitFeld selbst definieren
 * und damit fuer "NVIC_SetPriorityGrouping()" geeignet sind.
 *
 * Cortex-M3&M4 (STM32) erwarten den PriorityGrouping-Wert im Bereich 0,..,7
 * "pre-emption priority" ist ein veralteter Name fuer "group priority".
 * 
 * Fuer weitere Informationen siehe:
 *  {> Guide: chap7.4 }
 *  {> misc.h }
 *  {> core_cm3.h und dort NVIC_SetPriorityGrouping() }
 */
#define  STM32_NVIC_PriorityGroup_0   ((uint32_t)0x7)           /*!< 0 bits for pre-emption priority 4 bits for subpriority */
#define  STM32_NVIC_PriorityGroup_1   ((uint32_t)0x6)           /*!< 1 bits for pre-emption priority 3 bits for subpriority */
#define  STM32_NVIC_PriorityGroup_2   ((uint32_t)0x5)           /*!< 2 bits for pre-emption priority 2 bits for subpriority */
#define  STM32_NVIC_PriorityGroup_3   ((uint32_t)0x4)           /*!< 3 bits for pre-emption priority 1 bits for subpriority */
#define  STM32_NVIC_PriorityGroup_4   ((uint32_t)0x3)           /*!< 4 bits for pre-emption priority 0 bits for subpriority */


/*
 * timer setup
 * ============
 */
#define  SYS_FREQ     168000000                                     // 168MHz system frequency
#define  TIMER_FREQ       44106                                     // 44.1 KHz requested timer frequency

/*
 * MODES
 */
#define MODE_SINE 0
#define MODE_SAW  1

/*
 * CONVERT Q15 TO Q12 BY SHIFTING BY 3
 */
#define CONVERT_Q15_Q12 3

/*
 * SHIFT RIGHT BY 1 (divide by 2) TO GET AMPLITUDE 1.0V AND 0.5V
 */
#define FULL_AMP 0
#define HALF_AMP 1

/*
 * FIFO for data handover from "task" to ISR
 * ====
 */
//
// FIFO_BUFF_SIZE has to equal 2^n                                     (for FIFO_INDX_MSK)
#define   FIFO_BUFF_SIZE   ( b(1) << 2 )                            /*!< 4 entries fifo depth resp. buffer size */
//const int FIFO_BUFF_SIZE = 0x4;                                   // ab GCC 4.3 ist "const" i.d.R. dem "#define" vorzuziehen
//
// FIFO INDeX MaSK ;      "& ((2^n)-1)"  <=> "% ((2^n)-1)              FIFO-INDeX-MaSK WONT work if FIFO-BUFF-SIZE does NOT equal 2^n !!!
#define   FIFO_INDX_MSK   ( FIFO_BUFF_SIZE - 1 )
//const int FIFO_INDX_MSK = FIFO_BUFF_SIZE - 1;                     // ab GCC 4.3 ist "const" i.d.R. dem "#define" vorzuziehen
//
static volatile uint16_t fifo[ FIFO_BUFF_SIZE ] = { 0 };            // FIFO BUFFER
static volatile uint16_t fifoRdIndx = 0;                            // FIFO ReaD  INDeX
static volatile uint16_t fifoWrIndx = 0;                            // FIFO WRite INDeX



/*
 * MARKER
 * ======
 *
 * In der ISR sollte so kurz wie moeglich sein. Zugriff auf externe Peripherie kostet Zeit.
 * In der ISR werden (u.U.) Marker gesetzt.
 * Der Main-Loop wertet die Marker aus und reagiert entsprechend.
 * In der Konsequenz ist die ISR kurz und fuer den Main-Loop kann bewusst geplant werden, wann die Zeit fuer die noetigen Dinge aufgebracht wird.
 * Dies kann das Einhalten von Echtzeitanforderungen erleichtern.
 */
static volatile int8_t   fifoUnderflow = 0;                         // "FIFO buffer UNDERFLOW was detected" state
static volatile int8_t   spuriousInterrupt = 0;                     // "spurious interrupt was detected" state

// SIGnal TABle Number Of Entries
#define   SIG_TAB_NOE   ( 360 )
//
// SIGNAL TABLE
static int16_t signalTable_saw[SIG_TAB_NOE] = { // normalized sawtooth signal values for 1.0V
	0
,182
,364
,546
,728
,910
,1092
,1274
,1456
,1638
,1820
,2002
,2184
,2366
,2548
,2730
,2912
,3094
,3276
,3458
,3640
,3822
,4004
,4186
,4369
,4551
,4733
,4915
,5097
,5279
,5461
,5643
,5825
,6007
,6189
,6371
,6553
,6735
,6917
,7099
,7281
,7463
,7645
,7827
,8009
,8191
,8373
,8555
,8738
,8920
,9102
,9284
,9466
,9648
,9830
,10012
,10194
,10376
,10558
,10740
,10922
,11104
,11286
,11468
,11650
,11832
,12014
,12196
,12378
,12560
,12742
,12924
,13107
,13289
,13471
,13653
,13835
,14017
,14199
,14381
,14563
,14745
,14927
,15109
,15291
,15473
,15655
,15837
,16019
,16201
,16383
,16565
,16747
,16929
,17111
,17293
,17476
,17658
,17840
,18022
,18204
,18386
,18568
,18750
,18932
,19114
,19296
,19478
,19660
,19842
,20024
,20206
,20388
,20570
,20752
,20934
,21116
,21298
,21480
,21662
,21845
,22027
,22209
,22391
,22573
,22755
,22937
,23119
,23301
,23483
,23665
,23847
,24029
,24211
,24393
,24575
,24757
,24939
,25121
,25303
,25485
,25667
,25849
,26031
,26214
,26396
,26578
,26760
,26942
,27124
,27306
,27488
,27670
,27852
,28034
,28216
,28398
,28580
,28762
,28944
,29126
,29308
,29490
,29672
,29854
,30036
,30218
,30400
,30583
,30765
,30947
,31129
,31311
,31493
,31675
,31857
,32039
,32221
,32403
,32585
,-32767
,-32585
,-32403
,-32221
,-32039
,-31857
,-31675
,-31493
,-31311
,-31129
,-30947
,-30765
,-30583
,-30400
,-30218
,-30036
,-29854
,-29672
,-29490
,-29308
,-29126
,-28944
,-28762
,-28580
,-28398
,-28216
,-28034
,-27852
,-27670
,-27488
,-27306
,-27124
,-26942
,-26760
,-26578
,-26396
,-26214
,-26031
,-25849
,-25667
,-25485
,-25303
,-25121
,-24939
,-24757
,-24575
,-24393
,-24211
,-24029
,-23847
,-23665
,-23483
,-23301
,-23119
,-22937
,-22755
,-22573
,-22391
,-22209
,-22027
,-21845
,-21662
,-21480
,-21298
,-21116
,-20934
,-20752
,-20570
,-20388
,-20206
,-20024
,-19842
,-19660
,-19478
,-19296
,-19114
,-18932
,-18750
,-18568
,-18386
,-18204
,-18022
,-17840
,-17658
,-17476
,-17293
,-17111
,-16929
,-16747
,-16565
,-16383
,-16201
,-16019
,-15837
,-15655
,-15473
,-15291
,-15109
,-14927
,-14745
,-14563
,-14381
,-14199
,-14017
,-13835
,-13653
,-13471
,-13289
,-13107
,-12924
,-12742
,-12560
,-12378
,-12196
,-12014
,-11832
,-11650
,-11468
,-11286
,-11104
,-10922
,-10740
,-10558
,-10376
,-10194
,-10012
,-9830
,-9648
,-9466
,-9284
,-9102
,-8920
,-8738
,-8555
,-8373
,-8191
,-8009
,-7827
,-7645
,-7463
,-7281
,-7099
,-6917
,-6735
,-6553
,-6371
,-6189
,-6007
,-5825
,-5643
,-5461
,-5279
,-5097
,-4915
,-4733
,-4551
,-4369
,-4186
,-4004
,-3822
,-3640
,-3458
,-3276
,-3094
,-2912
,-2730
,-2548
,-2366
,-2184
,-2002
,-1820
,-1638
,-1456
,-1274
,-1092
,-910
,-728
,-546
,-364
,-182
};

static int16_t signalTable_sine[SIG_TAB_NOE] = { // normalized sine signal values for 1.0V
	0, 571, 1143, 1714, 2285, 2855, 3425, 3993, 4560, 5126, 5690, 6252, 6812, 7371, 7927, 8480, 9032, 9580, 10125, 10668, 11207,
 11743, 12275, 12803, 13327, 13848, 14364, 14876, 15383, 15886, 16383, 16876, 17364, 17846, 18323, 18794, 19260, 19720, 20173, 
 20621, 21062, 21497, 21926, 22347, 22762, 23170, 23571, 23964, 24351, 24730, 25101, 25465, 25821, 26169, 26509, 26841, 27165, 
 27481, 27788, 28087, 28377, 28659, 28932, 29196, 29451, 29697, 29935, 30163, 30381, 30591, 30791, 30982, 31164, 31336, 31498, 
 31651, 31794, 31928, 32051, 32165, 32270, 32364, 32449, 32523, 32588, 32643, 32688, 32723, 32748, 32763, 32767, 32763, 32748, 
 32723, 32688, 32643, 32588, 32523, 32449, 32364, 32270, 32165, 32051, 31928, 31794, 31651,
 31498, 31336, 31164, 30982, 30791, 30591, 30381, 30163, 29935, 29697, 29451, 29196, 28932, 28659, 28377, 28087, 27788,
27481, 27165, 26841, 26509, 26169, 25821, 25465, 25101, 24730, 24351, 23964, 23571, 23170, 22762, 22347, 21926, 21497, 
21062, 20621, 20173, 19720, 19260, 18794, 18323, 17846, 17364, 16876, 16383, 15886, 15383, 14876, 14364, 13848, 13327, 
12803, 12275, 11743, 11207, 10668, 10125, 9580, 9032, 8480, 7927, 7371, 6812, 6252, 5690, 5126, 4560, 3993, 3425, 2855, 
2285, 1714, 1143, 571, 0, -571, -1143, -1714, -2285, -2855, -3425, -3993, -4560, -5126, -5690, -6252, -6812, -7371, -7927,
 -8480, -9032, -9580, -10125, -10668, -11207, -11743, -12275, -12803, -13327, -13848, -14364, -14876, -15383, -15886, 
 -16384, -16876, -17364, -17846, -18323, -18794, -19260, -19720, -20173, -20621, -21062, -21497, -21926, -22347, -22762, 
 -23170, -23571, -23964, -24351, -24730, -25101, -25465, -25821, -26169, -26509, -26841, -27165, -27481, -27788, -28087, 
 -28377, -28659, -28932, -29196, -29451, -29697, -29935, -30163, -30381, -30591, -30791, -30982, -31164, -31336, -31498, 
 -31651, -31794, -31928, -32051, -32165, -32270, -32364, -32449, -32523, -32588, -32643, -32688, -32723, -32748, -32763, 
 -32768, -32763, -32748, -32723, -32688, -32643, -32588, -32523, -32449, -32364, -32270, -32165, -32051, -31928, -31794, 
 -31651, -31498, -31336, -31164, -30982, -30791, -30591, -30381, -30163, -29935, -29697, -29451, -29196, -28932, -28659, 
 -28377, -28087, -27788, -27481, -27165, -26841, -26509, -26169, -25821, -25465, -25101, -24730, -24351, -23964, -23571, 
 -23170, -22762, -22347, -21926, -21497, -21062, -20621, -20173, -19720, -19260, -18794, -18323, -17846, -17364, -16876, 
-16384, -15886, -15383, -14876, -14364, -13848, -13327, -12803, -12275, -11743, -11207, -10668, -10125, -9580, -9032, -8480, 
-7927, -7371, -6812, -6252, -5690, -5126, -4560, -3993, -3425, -2855, -2285, -1714, -1143, -571
};




//----------------------------------------------------------------------------
//
//  ISR
//

void TIM1_UP_TIM10_IRQHandler(void){                                // ISR for Timer1 IRQs
    if( TIM1->SR & TIM_SR_UIF ){                                    // is service actually requested?
        // yes, there is an actual request for this service
        // clear flag marking the IRQ
        // ==========
        
        TIM1->SR = ~TIM_SR_UIF;                                     // acknowledge interrupt resp. clear IRQ-flag to receive new interrupts   {>RM0090 chap 17.4.5}  _???_<150508> reference that writing clears bit
        
        // load new signal value from fifo buffer, align it and hand it over to DAC
        if ( fifoRdIndx != fifoWrIndx ){                            // check for fifo underflow
            // NO fifo underflow => required data is in fifo
            //
            // send data to DAC
            DAC->DHR12R1 = fifo[fifoRdIndx];                        // load current data value into DAC output register {>RM0090 chap 14.5.3}
            //
            // update FIFO ReaD INDeX
            fifoRdIndx = (fifoRdIndx + 1) & FIFO_INDX_MSK;          // cyclic increment
            
        } else {
            fifoUnderflow = -1;                                     // mark FIFO UNDERFLOW
        }
            
    } else {
        // service was NOT requested                                   if we end up here, who is doing acknowledge and where ??? => LOOP(!?)
        spuriousInterrupt = -1;                                     // mark SPURIOUS INTERRUPT
    }
}


//----------------------------------------------------------------------------
//
// function prototype(s)
//
void showInfoOnTFT(void);


//----------------------------------------------------------------------------
//
//  MAIN
//
int main( void ){

    // fifo specific
    uint16_t fifoNextWrIndx  =  0;                                      // FIFO NEXT WRite INDeX

    // signal table specific
	const uint8_t QFRACTIONBITS = 22;
    const int64_t LIMIT         =  SIG_TAB_NOE << QFRACTIONBITS;            // shift left for increased precision                                          ;  Q9.22
    const int64_t DELTA440      =  (440 * LIMIT) / TIMER_FREQ;// + (1 << 17);   // delta for index (with increased precision)                                  ;  Q9.22
    const int64_t DELTA5000     =  (5000 * LIMIT) / TIMER_FREQ;// + (3 << 23);  // delta for index (with increased precision)                                  ;  Q9.22
    const int64_t MAX_INDX440   =  LIMIT - (2 * DELTA440) + 1;              // (min. value of) MAXimum INDeX (with increased precision)                    ;  Q9.20
    const int64_t MAX_INDX5000  =  LIMIT - (2 * DELTA5000) + 1;             // (min. value of) MAXimum INDeX (with increased precision)                    ;  Q9.20
    const int64_t q15_offset  = (1.5 * 4095) / 3.3;                         // Q15 offset, so DAC doesn't overflow (waves not displayed correctly)         ;  Q15
    
    int32_t sigTabIndx          =  0;                                   // SIGnal TABle INDeX ; initialize with start position                         ;  Q9.20
    
    
    uint8_t pinPos;                                                     // PIN POSition (when configuring GPIO)
    
    // mode (sine / sawtooth)
    uint8_t mode        = (int32_t)MODE_SINE;
    int32_t max_index   = (int32_t)MAX_INDX440;
    int32_t delta       = (int32_t)DELTA440;
    uint8_t amplitude   = (int32_t)FULL_AMP;
	
    
    /*
     * general setup
     * =============
     */
    initCEP_Board();                                                // initilize display, leds, buttons, uart and other stuff CE_Lib.c
    showInfoOnTFT();    
    
    
    /* clock setup
     * ===========
     *
     * switch on needed components
     *
     * Fuer alle benoetigten Komponenten wie Ports, Timer und DAC wird der CLK angeschaltet.
     * Hierfuer muss auf das RCC (Reset and Clock Control) Register konfiguriert werden.
     * AHB ::= Advanced High-performance Bus    (protocol as part of AMBA specification)
     * APB ::= Advanced Peripheral Bus          (protocol as part of AMBA specification)
     * AMBA ::= Advanced Microcontroller Bus Architecture
     *
     * "stm32f4xx.h" definiert
     *   den Pointer
     *      RCC   auf den Memory-Mapped Reset and Clock Control block               siehe Abschnitt: "Peripheral_declaration"
     *   die zugehoerigen Register
     *      AHB1ENR ::= AHB1 peripheral clock ENable Register                       siehe struct-Definiton: "RCC_TypeDef"
     *      AHB2ENR ::= AHB2 peripheral clock ENable Register                       siehe struct-Definiton: "RCC_TypeDef"
     *   die zugehoerigen Bit-Definitionen
     *      RCC_AHB1ENR_DACEN                                                       siehe Abschnitt: "Bit definition for RCC_AHB1ENR register"
     *      RCC_AHB1ENR_GPIO*EN                                                     siehe Abschnitt: "Bit definition for RCC_AHB1ENR register"
     *      RCC_AHB2ENR_TIM*EN                                                      siehe Abschnitt: "Bit definition for RCC_AHB2ENR register"
     */
    RCC->AHB1ENR |= (   RCC_AHB1ENR_GPIOAEN                         // enable clock for GPIOA                           {>RM0090 chap 6.3.10}
                      | RCC_AHB1ENR_GPIOCEN                         // enable clock for GPIOC                           {>s.a.}
                      | RCC_AHB1ENR_GPIOFEN                         // enable clock for GPIOF                           {>s.a.}
                      | RCC_AHB1ENR_GPIOHEN                         // enable clock for GPIOH                           {>s.a.}
                      | RCC_AHB1ENR_GPIOIEN                         // enable clock for GPIOI                           {>s.a.}
    );
    //
    RCC->APB1ENR |= RCC_APB1ENR_DACEN;                              // enable clock for DAC                             {>RM0090 chap 6.3.13}
    //
    RCC->APB2ENR |= RCC_APB2ENR_TIM1EN;                             // enable clock for timer 8                         {>RM0090 chap 6.3.14}
    
    
    
    /*
     * LED setup (actually already initialised/done by initCEP_Board()
     * =========
     *
     * LED7 ::= OK - NO fifo underflow since start                  ( connected to GPIO I.7 )
     * LED6 ::= ERROR - at least one fifo undeflow since start      ( connected to GPIO I.6 )
     * LED5 ::= false interrupt                                     ( connected to GPIO I.5 )
     *
     * "stm32f4xx.h" definiert
     *   den Pointer
     *      GPIOI   auf den Memory-Mapped "GPIO-I Block"              siehe Abschnitt: "Peripheral_declaration"
     *   die zugehoerigen Register
     *      MODER   ::= gpio port MODE Register                       siehe struct-Definiton: "GPIO_TypeDef"
     *      OSPEEDR ::= gpio port Output SPEED Register               siehe struct-Definiton: "GPIO_TypeDef"
     *      OTYPER  ::= gpio port Output TYPE Register                siehe struct-Definiton: "GPIO_TypeDef"
     *      PUPDR   ::= gpio port Pull-Up/Pull-Down Register          siehe struct-Definiton: "GPIO_TypeDef"
     * "stm32f4xx_gpio.h" definiert
     *   die zugehoerigen Bit-Definitionen
     *      GPIO_Low_Speed (frueher/veraltet: GPIO_Speed_2MHz)        siehe enum-Definiton: "GPIOSpeed_TypeDef" (GPIO Output Maximum frequency enumeration)
     *      GPIO_Mode_OUT                                             siehe enum-Definiton: "GPIOMode_TypeDef"  (GPIO Configuration Mode enumeration)
     *      GPIO_OType_PP                                             siehe enum-Definiton: "GPIOOType_TypeDef" (GPIO Output type enumeration)
     *      GPIO_PuPd_NOPULL                                          siehe enum-Definiton: "GPIOPuPd_TypeDef"  (GPIO Configuration PullUp PullDown enumeration)
     *
     * Fuer weitere Informationen siehe:
     *  {> TI2 GS }
     *  {> RM0090 chap 8.3&4 }
     */
    for( pinPos=4; pinPos<=7; pinPos++ ){
        GPIOI->OTYPER  = (GPIOI->OTYPER  & ~( b(11) << (pinPos*2))) | (GPIO_OType_PP    << (pinPos*2));             //  {>RM0090 chap 8.4.2}
        GPIOI->OSPEEDR = (GPIOI->OSPEEDR & ~( b(11) << (pinPos*2))) | (GPIO_Low_Speed   << (pinPos*2));             //  {>RM0090 chap 8.4.3}
        GPIOI->PUPDR   = (GPIOI->PUPDR   & ~( b(11) << (pinPos*2))) | (GPIO_PuPd_NOPULL << (pinPos*2));             //  {>RM0090 chap 8.4.4}
        GPIOI->MODER   = (GPIOI->MODER   & ~( b(11) << (pinPos*2))) | (GPIO_Mode_OUT    << (pinPos*2));             //  {>RM0090 chap 8.4.1}
    }//for
    
    
    
    /*
     * Timer setup
     * ===========
     *
     * "stm32f4xx.h" definiert
     *   den Pointer
     *      TIM1  auf den MemoryMapped Timer1                       siehe Abschnitt: "Peripheral_declaration"
     *   die zugehoerigen Register
     *      ARR  ::= AutoReloadRegister                             siehe struct-Definiton: "TIM_TypeDef"
     *      CR1  ::= ControlRegister1                               siehe struct-Definiton: "TIM_TypeDef"
     *      CR2  ::= ControlRegister2                               siehe struct-Definiton: "TIM_TypeDef"
     *      DIER ::= Dma/InterruptEnableRegister                    siehe struct-Definiton: "TIM_TypeDef"
     *      PSC  ::= PreSCaler                                      siehe struct-Definiton: "TIM_TypeDef"
     *   die zugehoerigen Bit-Definitionen
     *      TIM_CR1_ARPE ::= Auto-Reload Preload Enable             siehe Abschnitt: "TIM / Bit definition for TIM_CR1 register"
     *      TIM_DIER_UIE ::= Update Interrupt Enable                siehe Abschnitt: "TIM / Bit definition for TIM_DIER register"
     *
     * Fuer weitere Informationen siehe:
     *  {> TI2 GS }
     *  {> RM0090 chap 17 }
     */
    TIM1->CR1 = 0;                                                  // disabled timer                   {>RM0090 chap 17.4.1)  
    TIM1->CR2 = 0;                                                  //                                  {>RM0090 chap 17.4.2}
    TIM1->PSC = 1    -1;                                            // NO prescaler                     {>RM0090 chap 17.4.11}
    TIM1->ARR = (SYS_FREQ / TIMER_FREQ)   -1;                       // 48000Hz                          {>RM0090 chap 17.4.12}
    TIM1->DIER = TIM_DIER_UIE;                                      // enable interrupt                 {>RM0090 chap 17.4.4}
    TIM1->CR1 = TIM_CR1_ARPE;                                       // auto-reload preload enable       {>RM0090 chap 17.4.1}
    
    
    
    /*
     * NVIC ((Nested Vector) Interrupt Controller) specific settings                                    {>PM0214 chap 2.3 & 4.3.10 & 4.4.5}
     * ====
     *
     * Achtung kleiner Prioritaetswerte haben hoehere Dringlichkeit/Prioritaet
     *
     * Hier muessen
     *   das Priority Grouping eingestellt werden.
     *     Also im Falle des STM32F417, wieviele der 4Bit jeweils die Pre-Emption Priority und wieviele die Sub-Priority bestimmen
     *   die konkrete Priority bzw. die onkreten Werte fuer Pre-Emption Priority und Sub-Priority
     *   die Interrupts freigeschaltet werden
     *
     * Das Prozessor-spezifische Header-File "core_cm3.h" (als Teilstueck des CMSIS-Core) definiert
     *   die Funktionen
     *      NVIC_EnableIRQ()                                        siehe Abschnitt: "NVIC functions"
     *      NVIC_SetPriority()                                      siehe Abschnitt: "NVIC functions"
     *      NVIC_SetPriorityGrouping()                              siehe Abschnitt: "NVIC functions"
     * "stm32f4xx.h" definiert
     *   die zugehoerigen Bit-Definitionen
     *      TIM1_UP_TIM10_IRQn                                      siehe enum-Definiton: "IRQn_Type"  (STM32F4XX Interrupt Number Definition(, according to the selected device))
     * Das zuvor selbstdefinierte Token
     *      STM32_NVIC_PriorityGroup_2                              ist "angelehnt" an NVIC_PriorityGroup_2 aus misc.h
     *
     * Fuer weitere Informationen siehe:
     *  {> Guide: chap7.3&4 }
     */
    NVIC_SetPriorityGrouping( STM32_NVIC_PriorityGroup_2 );         // 2bit preEmptionPrio & 2bit subPrio
    NVIC_SetPriority( TIM1_UP_TIM10_IRQn, 4 );                      // preEmptionPrio:=1 & subPrio:=0
    NVIC_EnableIRQ( TIM1_UP_TIM10_IRQn );                           // enable IRQ
    
    
    
    /*
     * Digital Analog Converter (DAC)                                                                                   {>RM0090 chap 14}
     * ==============================
     *
     * Set GPIOA.4 to ANalog for use with DAC                                                                           {>RM0090 chap 8.3.2, Peripheral alternate function   & stm32f4xx.h}
     *
     * "stm32f4xx.h" definiert
     *   den Pointer
     *      GPIOA   auf den Memory-Mapped "GPIO-A Block"              siehe Abschnitt: "Peripheral_declaration"
     *   die zugehoerigen Register
     *      MODER   ::= gpio port MODE Register                       siehe struct-Definiton: "GPIO_TypeDef"
     * "stm32f4xx_gpio.h" definiert
     *   die zugehoerigen Bit-Definitionen
     *      GPIO_PinSource4 ::= PIN #4 is SOURCE (hier => GPIO A4)    siehe Abschnitt: "GPIO_Pin_sources"
     *      GPIO_Mode_AN    ::= Analog Mode                           siehe enum-Definiton: "GPIOMode_TypeDef"  (GPIO Configuration Mode enumeration)
     *
     * "stm32f4xx.h" definiert
     *   den Pointer
     *      DAC   auf den Memory-Mapped "Digital Aanalog Converter block"   siehe Abschnitt: "Peripheral_declaration"
     *   die zugehoerigen Register
     *      CR ::= Control Register                                   siehe struct-Definiton: "DAC_TypeDef"
     *   die zugehoerigen Bit-Definitionen
     *      GPIO_PinSource4 ::= PIN #4 is SOURCE (hier => GPIO A4)    siehe Abschnitt: "Digital to Analog Converter / Bit definition for DAC_CR register"
     *
     * Fuer weitere Informationen siehe:
     *  {> TI2 GS }
     *  {> RM0090 chap 8 }
     *  {> RM0090 chap 14 }
     */
    GPIOA->MODER = (GPIOA->MODER & ~(b(11) << (GPIO_PinSource4 * 2))) | (GPIO_Mode_AN << (GPIO_PinSource4 * 2));
    DAC->CR = 0;                                                    // ControlRegister: configure / keep defaults       {>RM0090 chap 14.5.1}
    DAC->CR |= DAC_CR_EN1;                                          // ControlRegister: Enable DAC channel1             {>RM0090 chap 14.5.1}
    
    // fill fifo
    sigTabIndx = 0;
    do {
        // write into fifo
        fifoWrIndx = fifoNextWrIndx;                                // update FIFO Write INDeX
        if (mode == MODE_SINE)
            fifo[fifoWrIndx] = (signalTable_sine[sigTabIndx >> QFRACTIONBITS] >> CONVERT_Q15_Q12 >> amplitude)/3 + q15_offset;
        else if (mode == MODE_SAW)
            fifo[fifoWrIndx] = (signalTable_saw[sigTabIndx >> QFRACTIONBITS] >> CONVERT_Q15_Q12 >> amplitude)/3 + q15_offset;
        
        sigTabIndx =  (sigTabIndx + delta) % LIMIT;
        
        // update SIGNAL TABle INDeX
      /*  if (sigTabIndx < max_index) {                               // already at end of table?
            sigTabIndx += delta;                                    //   NO -> increment
        } else {
            sigTabIndx -= LIMIT;                                    //   YES -> start again at beginning
        }
        */
        // compute next FIFO WRite INDeX
        fifoNextWrIndx = (fifoWrIndx + 1) & FIFO_INDX_MSK;          // cyclic increment
        
    } while( fifoNextWrIndx != fifoRdIndx );                        // stop when buffer filled
    
    
    
    /*
     * "stm32f4xx.h" definiert
     *   den Pointer
     *      GPIOI   auf den Memory-Mapped "GPIO-I Block"              siehe Abschnitt: "Peripheral_declaration"
     *   die zugehoerigen Register
     *      BSRRL ::= gpio port Bit Set[/Reset] Register [Low]        siehe struct-Definiton: "GPIO_TypeDef"
     *      BSRRH ::= gpio port Bit [Set/]Reset Register [High]       siehe struct-Definiton: "GPIO_TypeDef"
     *
     * Fuer weitere Informationen siehe:
     *  {> TI2 GS }
     */
    // initialize LED(s)
    GPIOI->BSRRL =  b(1) << 7;                                      // set OK - NO fifo underflow yet
    GPIOI->BSRRH = b(11) << 5;                                      // NO fifo underflow yet  and  NO spurious interrupt yet
    
    
    
    /*
     * "stm32f4xx.h" definiert
     *   den Pointer
     *      TIM1  auf den MemoryMapped Timer8                       siehe Abschnitt: "Peripheral_declaration"
     *   die zugehoerigen Register
     *      CR1  ::= ControlRegister1                               siehe struct-Definiton: "TIM_TypeDef"
     *   die zugehoerigen Bit-Definitionen
     *      TIM_CR1_CEN ::= Counter ENable                          siehe Abschnitt: "TIM / Bit definition for TIM_CR1 register"
     */
    // start action by starting timer
    TIM1->CR1 |= TIM_CR1_CEN;                                       // start timer                    {>RM0090 chap 17.3.4}
    // NOW read accesses on fifo are starting    
    GPIOI->BSRRL =  b(1) << 4;                                      // mark on LED that timer has started
    
    
    
    /*
     * Es folgt der Main-Loop.
     * Dier prueft der Reihe nach, ob ein Task zu erledigen ist und falls dies der Fall ist, arbeitet er den jeweiligen Task ab.
     * Die Summe der Ausfuehrungszeiten aller Tasks ist der WorstCase und bestimmt die Reaktionszeit, die zugesichert werden kann.
     */
    // NEVER stop doing task(s)
    while(1){
        // Es wird zyklisch geprueft, ob ein Task zu tun ist und falls ja, dann wird er getan
        
        /*
         * Der 1.Task:
         */
        // keep fifo filled
        if( fifoNextWrIndx != fifoRdIndx ){                         // is fifo full?
            // there is still place left inside fifo for data
            
            // write into fifo
            fifoWrIndx = fifoNextWrIndx;                            // update FIFO Write INDeX
        
        if (mode == MODE_SINE)
            fifo[fifoWrIndx] = (signalTable_sine[sigTabIndx >> QFRACTIONBITS] >> CONVERT_Q15_Q12 >> amplitude)*10/33 + q15_offset;
        else if (mode == MODE_SAW)
            fifo[fifoWrIndx] = (signalTable_saw[sigTabIndx >> QFRACTIONBITS] >> CONVERT_Q15_Q12 >> amplitude)*10/33 + q15_offset;
            
         sigTabIndx =  (sigTabIndx + delta) % LIMIT;
        
            // update SIGNAL TABle INDeX
            /*if (sigTabIndx < max_index) {                           // already at end of table?
                sigTabIndx += delta;                                // NO -> increment
            }else{                                                  //
                sigTabIndx -= LIMIT;                            // YES -> start again at beginning (for accurate frequency NOT round rotating)
            }
        */
            
            // compute next FIFO WRite INDeX
            fifoNextWrIndx = (fifoWrIndx + 1) & FIFO_INDX_MSK;      // cyclic increment
        }
        
        
        // in case of fifo underflow  signal it
        if( fifoUnderflow ){
            GPIOI->BSRRH = b(1) << 7;                               // clear OK, since fifo underflow detected
            GPIOI->BSRRL = b(1) << 6;                               // mark ERROR (fifo underflow was detected)
        }//if
        
        
        if( spuriousInterrupt ){
            GPIOI->BSRRL = b(1) << 5;                               // mark ERROR (spurious interrupt was detected)
        }//if
        
        if (S8) {                       // 5000 Hz frequency
            delta = (int32_t)DELTA5000;
            max_index = (int32_t)MAX_INDX5000;   // Set maximum index for LUT to 5000Hz
        }
        
        if (S7) {                       // 440 Hz frequency
            delta = (int32_t)DELTA440;
            max_index = (int32_t)MAX_INDX440;    // Set maximum index for LUT to 440Hz
        }
        
        if (S6) {                   // Change to sine wave mode
            mode = MODE_SINE;
        }
        
        if (S5) {                   // Change to sawtooth wave mode
            mode = MODE_SAW;
        }
        
        if (S4) {                   // 1.0V amplitude (full amplitude)
            amplitude = FULL_AMP;
        }
        
        if (S3) {                   // 0.5V amplitude (half amplitude)
            amplitude = HALF_AMP;
        }
        
        if (S2) {
            TIM1->CR1 = TIM_CR1_CEN;                                // start timer
            GPIOI->ODR ^= b(0) << 4;                                // update related LED
        }
        
        if (S1) {
            TIM1->CR1 &= ~TIM_CR1_CEN;                              // stop timer
            GPIOI->ODR ^= b(0) << 4;                                // update related LED
        }
        
    }
}

/*
 * TFT info
 * ========
 *
 * Das TFT-Display laeuft ueber einen I2C-Bus.
 * Der I2C-Bus ist nicht schnell genug um beliebig viele Zeichen mit "�C-Full-Speed" zu uebertragen
 * Um das Beispiel NICHT unnoetig auszublaehen, wird auf eine saubere Loesung verzichtet, als da waeren
 *   Status des "I2C-Blocks" abfragen
 *   Timer programmieren, der korrektes Tempo sicherstellt und diesen auswerten
 * Sondern
 *   in einer Schleife Zeit vertroedelt.
 * Achtung! In serioesen Code ist das eine GANZ SCHLECHTE Idee und NICHT zulaessig!
 * Timer wurden bewusst eingefuehrt um genau so etwas zu vermeiden.
 *
 * Fuer weitere Informationen siehe:
 *  {> TI2 GS }
 *  {> tft.h }
 */
void showInfoOnTFT(void){
    uint8_t lineCntTFT;                                             // Y-position on TFT
    uint64_t dawdle;                                                // to DAWDLE away time - ATTENTION: this is VERY BAD coding style ;  _???_<150522> find better solution that is well suited for beginners
    
    TFT_cls();
    lineCntTFT = 1;
    TFT_gotoxy( 1, lineCntTFT );
    TFT_puts( "CE-Aufgabe 3" );
    lineCntTFT++;
    TFT_gotoxy( 1, lineCntTFT );
    TFT_puts( "Sending waveform via ISR to DAC" );
    lineCntTFT++;
    TFT_gotoxy( 1, lineCntTFT );
    TFT_puts( THE_VERY_C_CODE_VERSION );
    lineCntTFT+=2;
    for( dawdle=0; dawdle<1000000; dawdle++ );                      // dawdle away time, since I2C is too slow - ATTENTION: this is VERY BAD coding style ;  _???_<150522> find better solution that is well suited for beginners
    //
    TFT_gotoxy( 1, lineCntTFT );
    TFT_puts( "Switch S8: 5000Hz Freq" );
    lineCntTFT++;
    TFT_gotoxy( 1, lineCntTFT );
    TFT_puts( "Switch S7: 440Hz  Freq" );
    lineCntTFT++;
    TFT_gotoxy( 1, lineCntTFT );
    TFT_puts( "Switch S6: Sine Wave" );
    lineCntTFT++;
    TFT_gotoxy( 1, lineCntTFT );
    TFT_puts( "Switch S5: Saw Tooth" );
    lineCntTFT++;
    TFT_gotoxy( 1, lineCntTFT );
    TFT_puts( "Switch S4: 1.0V amplitude" );
    lineCntTFT++;
    for( dawdle=0; dawdle<1000000; dawdle++ );                      // dawdle away time, since I2C is too slow - ATTENTION: this is VERY BAD coding style ;  _???_<150522> find better solution that is well suited for beginners
    //
    TFT_gotoxy( 1, lineCntTFT );
    TFT_puts( "Switch S3: 0.5V amplitude" );
    lineCntTFT++;
    TFT_gotoxy( 1, lineCntTFT );
    TFT_puts( "Switch S2: start timer" );
    lineCntTFT++;
    TFT_gotoxy( 1, lineCntTFT );
    TFT_puts( "Switch S1: stop timer" );
    lineCntTFT++;
    for( dawdle=0; dawdle<1000000; dawdle++ );                      // dawdle away time, since I2C is too slow - ATTENTION: this is VERY BAD coding style ;  _???_<150522> find better solution that is well suited for beginners
    //
    TFT_gotoxy( 1, lineCntTFT );
}//showInfoOnTFT()
